
import argparse
import json

from joblib import dump
import numpy as np
from sklearn import svm


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', type=str)
    parser.add_argument('-d', type=str)
    args = parser.parse_args()

    model_file = args.m
    data_file = args.d

    with open(data_file, 'r') as f:
        data = json.load(f)

    X = np.array(data['samples']['X'])
    y = np.array(data['samples']['y'])

    clf = svm.SVC()
    clf.fit(X, y)
    dump(clf, model_file)


if __name__ == '__main__':
    main()
