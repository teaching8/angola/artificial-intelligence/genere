import json
import argparse

from joblib import load
import numpy as np
from sklearn import svm


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', nargs=3, type=int)
    parser.add_argument('-m', type=str)
    parser.add_argument('-d', type=str)
    args = parser.parse_args()

    point = list(args.p)
    model_file = args.m
    data_file = args.d

    clf = load(model_file)
    with open(data_file, 'r') as f:
        data = json.load(f)

    classes = data['classes']
    prediction = clf.predict([point])[0]
    gender = classes[str(prediction)]

    print(gender)


if __name__ == '__main__':
    main()
